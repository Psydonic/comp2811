#include "video_grid_layout.h"
#include "video_button.h"
#include <math.h>

void Video_grid_layout::setGeometry(const QRect &r) {
    QLayout::setGeometry(r);
    int B = 5;
    int TILE_SX = 200;
    int TILE_SY = 110;
    int gridX = std::max(1,(r.width()-B)/(TILE_SX+B));
    int tileSizeX = (r.width()-((gridX+1)*B))/gridX;

    for (int i = 0; i < list_.size(); i++) {
        int tileSizeY = (TILE_SY/(float)TILE_SX)*tileSizeX;

        QLayoutItem *o = list_.at(i);
        try {
            VideoButton *button = static_cast<VideoButton *>(o->widget());
            if (button == NULL) // null: cast failed on pointer
                qDebug() << "warning, unknown widget class in layout";
            button->setGeometry(r.x()+((i%gridX)*(tileSizeX+B)+B),
                                r.y()+((i/gridX)*(tileSizeY+B)+B),tileSizeX,tileSizeY);
            button->setSize(tileSizeX,tileSizeY);
        }  catch (std::bad_cast) {
            qDebug() << "warning, unknown widget class in layout";
        }
    }
    height = ((list_.size()/gridX)+1)*(TILE_SY/(float)TILE_SX)*tileSizeX;
}

// following methods provide a trivial list-based implementation of the QLayout class
int Video_grid_layout::count() const {
    return list_.size();
}

QLayoutItem *Video_grid_layout::itemAt(int idx) const {
    return list_.value(idx);
}

QLayoutItem *Video_grid_layout::takeAt(int idx) {
    return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0;
}

void Video_grid_layout::addItem(QLayoutItem *item) {
    list_.append(item);
}

QSize Video_grid_layout::sizeHint() const {
    return minimumSize();
}

QSize Video_grid_layout::minimumSize() const {
    return QSize(contentsRect().width(),height + 10);
}

bool compTitle(QLayoutItem *v1, QLayoutItem *v2) {
    VideoButton *button1 = static_cast<VideoButton *>(v1->widget());
    VideoButton *button2 = static_cast<VideoButton *>(v2->widget());
    return button1->info->title < button2->info->title;
}

bool compDuration(QLayoutItem *v1, QLayoutItem *v2) {
    VideoButton *button1 = static_cast<VideoButton *>(v1->widget());
    VideoButton *button2 = static_cast<VideoButton *>(v2->widget());
    return button1->info->duration > button2->info->duration;
}

bool compDate(QLayoutItem *v1, QLayoutItem *v2) {
    VideoButton *button1 = static_cast<VideoButton *>(v1->widget());
    VideoButton *button2 = static_cast<VideoButton *>(v2->widget());
    return button1->info->title < button2->info->title;
}

bool visible(QLayoutItem *v1, QLayoutItem *v2) {
    VideoButton *button1 = static_cast<VideoButton *>(v1->widget());
    VideoButton *button2 = static_cast<VideoButton *>(v2->widget());
    return button1->isVisible() > button2->isVisible();
}

#include <algorithm>
void Video_grid_layout::sort(int index) {
    lastSortIndex = index;
    switch (index) {
        case 0:
            std::sort(list_.begin(), list_.end(), compTitle);
            break;
        case 1:
            std::sort(list_.begin(), list_.end(), compDuration);
            break;
        case 2:
            qDebug() << "Error sort by date not implimented yet";
            break;
    }
    std::sort(list_.begin(), list_.end(), visible);
    this->update();
}

void Video_grid_layout::search(QString text) {
    for (QLayoutItem* o: list_) {
        VideoButton *button = static_cast<VideoButton *>(o->widget());
        button->hide();
        if (text.isEmpty()) {
            button->show();
        }
        for (QString t: button->info->tags) {
            if (t.contains(text, Qt::CaseSensitivity::CaseInsensitive)) {
                button->show();
            }
        }
    }
    sort(lastSortIndex);
    std::sort(list_.begin(), list_.end(), visible);
    this->update();
}



