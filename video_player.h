//
// Created by twak on 11/11/2019.
//

#ifndef VIDEO_PLAYER_H
#define VIDEO_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "video_button.h"
#include <vector>
#include <QTimer>

using namespace std;

class VideoPlayer : public QMediaPlayer {

Q_OBJECT

private:
    QTimer* mTimer;
    long updateCount = 0;

public:
    VideoPlayer() : QMediaPlayer(NULL) {
        setVolume(0); // be slightly less annoying
        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), this, SLOT (playStateChanged(QMediaPlayer::State)) );
    }

    vector<VideoButton*>* buttons;

    // all buttons have been setup, store pointers here
    void setContent(vector<VideoButton*>* b);

private slots:

    void playStateChanged (QMediaPlayer::State ms);

public slots:

    // start playing this ButtonInfo
    void jumpTo (int index);
};

#endif //VIDEO_PLAYER_H
