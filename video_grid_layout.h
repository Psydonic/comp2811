#ifndef VIDEO_GRID_LAYOUT_H
#define VIDEO_GRID_LAYOUT_H

#include <QGridLayout>

class Video_grid_layout : public QGridLayout
{
public:
    Video_grid_layout() {}
    ~Video_grid_layout() {}

    // standard functions for a QLayout
    void setGeometry(const QRect &rect);

    void addItem(QLayoutItem *item);
    void sort(int index);
    void search(QString text);
    QSize sizeHint() const;
    QSize minimumSize() const;
    int count() const;
    QLayoutItem *itemAt(int) const;
    QLayoutItem *takeAt(int);
    int height;
private:
    QList<QLayoutItem*> list_;
    int lastSortIndex;
};

#endif // Video_grid_layout
