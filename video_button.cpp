//
// Created by twak on 11/11/2019.
//

#include "video_button.h"

void VideoButton::init(VideoButtonData* i, int ind) {
    index = ind;
    setIcon( *(i->icon) );
    info =  i;
}


void VideoButton::clicked() {
    emit jumpTo(index);
}


void VideoButton::setSize(int width, int height) {
    setFixedSize(width, height);
    setIconSize(QSize(width, height));
}

