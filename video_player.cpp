//
// Created by twak on 11/11/2019.
//

#include "video_player.h"
#include <QMediaPlaylist>
#include <QWaitCondition>

using namespace std;

// all buttons have been setup, store pointers here
void VideoPlayer::setContent(std::vector<VideoButton*>* b) {
    buttons = b;
    //add media and read tags
    for (VideoButton* i: *buttons) {
        playlist()->addMedia(*(i->info->url));
    }
}

void VideoPlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void VideoPlayer::jumpTo (int index) {
    playlist()->setCurrentIndex(index);
    play();
}

