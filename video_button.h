//
// Created by twak on 11/11/2019.
//

#ifndef VIDEO_BUTTON_H
#define VIDEO_BUTTON_H


#include <QPushButton>
#include <QUrl>
#include <QMediaPlayer>

class VideoButtonData {
public:
    QUrl* url; // video file to play
    QIcon* icon; // icon to display
    QStringList tags; // list of tags
    int duration; // duration in ms
    QString title; //base name

    VideoButtonData ( QUrl* url, QIcon* icon): url(url), icon(icon) {}
};

class VideoButton : public QPushButton {
    Q_OBJECT

public:
    VideoButtonData* info;

    VideoButton(QWidget *parent) :  QPushButton(parent) {
         connect(this, SIGNAL(released()), this, SLOT (clicked() )); // if QPushButton clicked...then run clicked() below
    }

    void init(VideoButtonData* i, int index);
    void setSize(int width, int height);

private:
    int index;

private slots:
    void clicked();

signals:
    void jumpTo(int);

};

#endif //VIDEO_BUTTON_H
